//
//  HideKB--Num Genegrator.swift
//  Cheat-Sheet
//
//  Created by Pooja Negi on 08/05/18.
//  Copyright © 2018 PNKBKSH. All rights reserved.
//

import UIKit

extension UIViewController{
    
    class func generateOrderIDWithPrefix(_ prefix: String) -> String {
        srandom(UInt32(time(nil)))
        let randomNo:Int   = Int(arc4random_uniform(50000))//just randomizing the number
        let orderID:String = "\(prefix)\(randomNo)"
        return orderID
    }
    
    
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }

}
