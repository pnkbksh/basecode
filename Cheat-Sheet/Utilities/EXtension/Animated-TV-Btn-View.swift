//
//  Animated-TV-Btn-View.swift
//  Cheat-Sheet
//
//  Created by Pooja Negi on 08/05/18.
//  Copyright © 2018 PNKBKSH. All rights reserved.
//

import UIKit

extension UIView{
    
    
    func animatedTable(_ tableName:UITableView){
        
        tableName.reloadData()
        let animetedCell    = tableName.visibleCells
        let tableViewHeight = tableName.bounds.size.height
        
        for cell in animetedCell{
            cell.transform = CGAffineTransform(translationX: 0, y: tableViewHeight)
        }
        
        var delayCounter = 0
        for cell in animetedCell{
            self.alpha = 0.5
            UIView.animate(withDuration: 1.6, delay: Double(delayCounter) * 0.09, usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: .curveEaseInOut, animations: {
                cell.transform = .identity
            }, completion: nil)
            delayCounter += 1
            self.alpha = 1.0
        }
    }
    
    func animatedCellView(allTable:UITableView){
        
        allTable.reloadData()
        var delayCounter    = 0
        let animetedCell    =  allTable.visibleCells
        let tableViewHeight = allTable.frame.size.height
        
        for cell in animetedCell{
            cell.transform = CGAffineTransform(translationX: 0, y: CGFloat(tableViewHeight))
        }
        for cell in animetedCell{
            self.alpha = 0.5
            UIView.animate(withDuration: 1.9, delay: 0.09, usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: .curveEaseInOut, animations: {
                cell.transform = .identity
                cell.frame.origin.y = 25 - CGFloat(delayCounter * 10)
                
            }, completion: nil)
            delayCounter += 1
            self.alpha = 1.0
        }
    }
    
    func animatedViews(views:[UIView]){
        var delayCounter = 0
        self.alpha       = 0.0
        for view in views{
            UIView.animate(withDuration: 1.9, delay: Double(delayCounter) * 0.05, usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: .curveEaseInOut, animations: {
                view.frame.origin.y = self.frame.origin.y - CGFloat(delayCounter * 15) // 20
            }, completion: nil)
            delayCounter += 1
            self.alpha = 1.0
        }
        self.layoutIfNeeded()
    }
}

extension UIButton{
    
    func animatedButton(){
        self.alpha = 0.0
        UIView.animate(withDuration: 1.7, delay: 0.03, usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: .curveEaseInOut, animations: {
            self.frame.origin.y     = self.frame.origin.y - 200
            self.transform          = .identity
        }, completion: {completion in
            self.alpha              = 1.0
        })
    }
}
