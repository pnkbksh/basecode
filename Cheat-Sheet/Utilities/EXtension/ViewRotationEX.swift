//
//  ViewRotationEX.swift
//  Cheat-Sheet
//
//  Created by Pooja Negi on 08/05/18.
//  Copyright © 2018 PNKBKSH. All rights reserved.
//

import UIKit

extension UIView {
    //Roate view 360 degree
    func rotate360Degrees(duration: CFTimeInterval = 1.5, completionDelegate: AnyObject? = nil) {
        
        let rotateAnimation         = CABasicAnimation(keyPath: "transform.rotation")
        rotateAnimation.fromValue   = 0.0
        rotateAnimation.toValue     = CGFloat(Double.pi * 2.0)
        rotateAnimation.duration    = duration
        rotateAnimation.repeatCount = Float.infinity
        
        DispatchQueue.main.async(execute: {
            if let delegate: AnyObject = completionDelegate {
                rotateAnimation.delegate = delegate as? CAAnimationDelegate
            }
            self.layer.add(rotateAnimation, forKey: nil)
        })
        
    }
    
}
