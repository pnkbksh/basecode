//
//  VC-Open-Animation.swift
//  Cheat-Sheet
//
//  Created by Pooja Negi on 08/05/18.
//  Copyright © 2018 PNKBKSH. All rights reserved.
//

import UIKit

extension UIViewController {
    
    // MARK: Alerts
    func showAlert(title: String?, message: String?, buttonTitle: String?){
        let alertVC     = UIAlertController(title: title != nil ? title! : nil, message: message != nil ? message! : nil, preferredStyle: .alert)
        alertVC.addAction(UIAlertAction(title: buttonTitle != nil ? buttonTitle : "Ok", style: UIAlertActionStyle.default, handler: nil))
        self.present(alertVC, animated: true, completion: nil)
    }
    
    func presentFadeAnimation(_ viewControllerToPresent: UIViewController){
        let transition      = CATransition()
        transition.duration = 0.25
        transition.type     = kCATransitionFade
        transition.subtype  = kCATransitionFromRight
        self.view.window?.layer.add(transition, forKey: kCATransition)
        present(viewControllerToPresent, animated: false)
    }
    
    func presentRightAnimation(_ viewControllerToPresent: UIViewController){
        
        let transition      = CATransition()
        transition.duration = 0.5
        transition.type     = kCATransitionReveal
        transition.subtype  = kCATransitionFromRight
        self.view.window?.layer.add(transition, forKey: kCATransition)
        present(viewControllerToPresent, animated: false)
    }
    
    func dissmissFadeView(){
        let transition      = CATransition()
        transition.duration = 0.5
        transition.type     = kCATransitionFade
        transition.subtype  = kCATransitionFromRight
        self.view.window?.layer.add(transition, forKey: kCATransition)
        dismiss(animated: false)
    }
    
    func dissmissToBottom(){
        let transition      = CATransition()
        transition.duration = 0.5
        transition.type     = kCATransitionPush
        transition.subtype  = kCATransitionFromBottom
        self.view.window!.layer.add(transition, forKey: kCATransition)
        dismiss(animated: false)
    }
    
    func dissmissToRight(){
        let transition      = CATransition()
        transition.duration = 0.25
        transition.type     = kCATransitionPush
        transition.subtype  = kCATransitionFromLeft
        self.view.window!.layer.add(transition, forKey: kCATransition)
        dismiss(animated: false)
    }
    
}





