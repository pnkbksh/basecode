//
//  Shimmer-Effect- Btn View.swift
//  Cheat-Sheet
//
//  Created by Pooja Negi on 08/05/18.
//  Copyright © 2018 PNKBKSH. All rights reserved.
//

import UIKit

func addGradientMaskToImage(view:UIImageView, transparency:CGFloat = 0.2, gradientWidth:CGFloat = 20.0) {
    
    let gradientMask        = CAGradientLayer()
    let gradientSize        = gradientWidth/view.frame.size.width
    let gradientColor       = UIColor(red: 174, green: 174, blue: 174, alpha: transparency)
    let startLocations      = [0, gradientSize/2, gradientSize]
    let endLocations        = [(1 - gradientSize), (1 - gradientSize/2), 1]
    let animation           = CABasicAnimation(keyPath: "locations")
    
    gradientMask.frame      = view.bounds
    gradientMask.colors     = [gradientColor.cgColor, UIColor.gray.cgColor, gradientColor.cgColor]
    gradientMask.locations  = startLocations as [NSNumber]?
    gradientMask.startPoint = CGPoint(x:0 - (gradientSize * 2), y: 0.3)
    gradientMask.endPoint   = CGPoint(x:1 + gradientSize, y: 0.9)
    view.layer.mask         = gradientMask
    animation.fromValue     = startLocations
    animation.toValue       = endLocations
    animation.repeatCount   = HUGE
    animation.duration      = 0.5
    gradientMask.add(animation, forKey: nil)
}


func addGradientMaskToLabel(view:UILabel, transparency:CGFloat = 0.2, gradientWidth:CGFloat = 20.0) {
    
    let gradientMask    = CAGradientLayer()
    let gradientSize    = gradientWidth/view.frame.size.width
    let gradientColor   = UIColor(red: 174, green: 174, blue: 174, alpha: transparency)
    let startLocations  = [0, gradientSize/2, gradientSize]
    let endLocations    = [(1 - gradientSize), (1 - gradientSize/2), 1]
    let animation       = CABasicAnimation(keyPath: "locations")
    
    gradientMask.frame      = view.bounds
    gradientMask.colors     = [gradientColor.cgColor, UIColor.gray.cgColor, gradientColor.cgColor,]
    gradientMask.locations  = startLocations as [NSNumber]?
    gradientMask.startPoint = CGPoint(x:0 - (gradientSize * 2), y: 0.4)
    gradientMask.endPoint   = CGPoint(x:1 + gradientSize, y: 0.7)
    view.layer.mask         = gradientMask
    animation.fromValue     = startLocations
    animation.toValue       = endLocations
    animation.repeatCount   = HUGE
    animation.duration      = 0.5
    gradientMask.add(animation, forKey: nil)
}

