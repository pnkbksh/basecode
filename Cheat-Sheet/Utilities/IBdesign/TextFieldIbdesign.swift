//
//  TextFieldIbdesign.swift
//  Cheat-Sheet
//
//  Created by Pooja Negi on 07/06/18.
//  Copyright © 2018 PNKBKSH. All rights reserved.
//

import Foundation
import  UIKit

extension UITextField{
    
    @IBInspectable var doneButton: Bool{
        get{
            return self.doneButton
        }
        set (isDone) {
            if isDone{
                doneButtonOnKeyboard()
            }
        }
    }
    
    func doneButtonOnKeyboard() {
        
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
        doneToolbar.barStyle = .default
        
        let cancel: UIBarButtonItem = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(self.cancelButtonAction))
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(self.doneButtonAction))
        
        let items = [cancel , flexSpace, done]
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        self.inputAccessoryView = doneToolbar
    }
    
    @objc func doneButtonAction() {
        self.resignFirstResponder()
    }
    
    @objc func cancelButtonAction() {
        self.text = ""
        self.resignFirstResponder()
    }
    
    @IBInspectable var nextTextfiled: Bool{
        get{
            return self.nextTextfiled
        }
        set (hasNext) {
            if hasNext{
                nextTextFieldCall()
            }
        }
    }
    
    func nextTextFieldCall(){
        if (self.superview?.viewWithTag(self.tag + 1) as? UITextField) != nil {
            self.becomeFirstResponder()
        } else {
            self.resignFirstResponder()
        }
        self.resignFirstResponder()
    }
}
