//
//  IB-design_Others.swift
//  Cheat-Sheet
//
//  Created by Pooja Negi on 08/05/18.
//  Copyright © 2018 PNKBKSH. All rights reserved.
//

import UIKit

//MARK:- all corner radius
@IBDesignable class AllCornerCurve: UIView{
    override func layoutSubviews() {
        super.layoutSubviews()
        updateCornerRadius()
    }
    @IBInspectable var rounded: Bool = false {
        didSet {
            updateCornerRadius()
        }
    }
    
    @IBInspectable var WIDTH: CGFloat = 10 {
        didSet { updateCornerRadius() }
    }
    
    @IBInspectable var HEIGHT: CGFloat = 10 {
        didSet { updateCornerRadius() }
    }
    
    func updateCornerRadius() {
        let path        = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: [.allCorners], cornerRadii: CGSize(width: WIDTH, height: HEIGHT))
        let mask        = CAShapeLayer()
        mask.path       = path.cgPath
        self.layer.borderColor = self.tintColor.cgColor
        self.layer.mask = mask
        self.layer.masksToBounds = false
    }
}


//MARK:- RectangleParabolaShape corner radius
@IBDesignable class RectangleParabolaShape: UIButton{
    override func layoutSubviews() {
        super.layoutSubviews()
        updateCornerRadius()
    }
    @IBInspectable var rounded: Bool = false {
        didSet {
            updateCornerRadius()
        }
    }
    func updateCornerRadius() {
        layer.cornerRadius          = rounded ? frame.size.height / 2 : 0
        self.layer.borderColor      = self.layer.backgroundColor
        self.layer.borderWidth      = 0.9
        self.layer.shadowRadius     = 2.0
        self.layer.shadowColor      = self.tintColor.cgColor
        self.layer.shadowOpacity    = 0.9
        self.layer.shadowOffset     = CGSize.init(width: 0.5, height: 2)
        self.layer.masksToBounds    = false
    }
}


//MARK:- halfParabolaShape corner radius to button
@IBDesignable class HalfHyperBolaShapeButton: UIButton{
   
    override func layoutSubviews() {
        super.layoutSubviews()
        updateRightSide()
    }
    
    @IBInspectable var right: Bool = false {
        didSet {
            updateRightSide()
        }
    }
    func updateRightSide(){
        //right border
        let path    = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: [.topRight, .bottomRight], cornerRadii: CGSize(width: self.frame.size.height / 2, height: self.frame.size.height / 2))
        let mask    = CAShapeLayer()
        mask.path   = path.cgPath
        self.layer.mask = mask
        self.layer.masksToBounds = false
    }
}


//MARK:- ImageCornerCurve corner radius
@IBDesignable class ImageCornerCurve: UIImageView{
    override func layoutSubviews() {
        super.layoutSubviews()
        updateImageCorner()
    }
    @IBInspectable var Corner: Bool = false {
        didSet {
            updateImageCorner()
        }
    }
    func updateImageCorner(){
        let path    = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: [.allCorners], cornerRadii: CGSize(width: 5, height: 5))
        let mask    = CAShapeLayer()
        mask.path   = path.cgPath
        self.layer.mask = mask
        self.layer.masksToBounds = false
    }
}


//MARK:- Label CornerCurve corner radius
@IBDesignable class LabelCornerCurve: UILabel{
    override func layoutSubviews() {
        super.layoutSubviews()
        updateImageCorner()
    }
    @IBInspectable var Corner: Bool = false {
        didSet {
            updateImageCorner()
        }
    }
    func updateImageCorner(){
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: [.allCorners], cornerRadii: CGSize(width: 2, height: 2))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
        self.layer.masksToBounds = false
    }
}


//MARK:- TopBorder ForView corner radius
@IBDesignable class TopBorderForView: UILabel{
    override func layoutSubviews() {
        super.layoutSubviews()
        updateLableTop()
    }
    @IBInspectable var top: Bool = false {
        didSet {
            updateLableTop()
        }
    }
    func updateLableTop(){
        let bottomBorder = CALayer()
        bottomBorder.frame = CGRect(x:0, y:self.frame.height - 1, width:self.frame.width + 30, height:1)
        bottomBorder.backgroundColor = self.backgroundColor?.cgColor
        self.layer.addSublayer(bottomBorder)
    }
}



//MARK:- Gredient color
@IBDesignable class RadialGradientView: UIView {
    
    @IBInspectable var outsideColor: UIColor = UIColor.black
    @IBInspectable var insideColor: UIColor = UIColor.gray
    
    override func draw(_ rect: CGRect) {
        let colors      = [insideColor.cgColor, outsideColor.cgColor] as CFArray
        let endRadius   = sqrt(pow(frame.width/2, 2) + pow(frame.height/2, 2))
        let center      = CGPoint(x: bounds.size.width / 2, y: bounds.size.height / 2)
        let gradient    = CGGradient(colorsSpace: nil, colors: colors, locations: nil)
        let context     = UIGraphicsGetCurrentContext()
        
        context?.drawRadialGradient(gradient!, startCenter: center, startRadius: 0.0, endCenter: center, endRadius: endRadius, options: CGGradientDrawingOptions.drawsBeforeStartLocation)
    }
}


//MARK:- Dotted Horizontal color
@IBDesignable class DottedHorizontal: UILabel {
    
    @IBInspectable var dotColor: UIColor = UIColor.white
    @IBInspectable var lowerHalfOnly: Bool = false
    
    override func draw(_ rect: CGRect) {
        let totalCount  = 15 + 15 - 1
        let fullwidth   = bounds.size.width
        let height      = bounds.size.height
        let itemLength  = fullwidth / CGFloat(totalCount)
        let path        = UIBezierPath()
        let beginFromTop = CGFloat(0.0)
        let top          = CGPoint(x: beginFromTop, y: height/2)
        let bottom       = CGPoint(x: fullwidth, y: height/2)
        
        path.move(to: top)
        path.addLine(to: bottom)
        path.lineWidth = height
        let dashes: [CGFloat] = [itemLength, itemLength]
        path.setLineDash(dashes, count: dashes.count, phase: 0)
        dotColor.setStroke()
        path.stroke()
    }
}



//MARK:- Hilighted button
@IBDesignable class HomeButtons: UIButton {
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        self.layer.cornerRadius = self.layer.frame.size.width/2
        self.layer.borderWidth  = 5.5
        self.layer.borderColor  = UIColor.black.cgColor
        self.backgroundColor    = UIColor.red
        self.setTitleColor(udYellow, for: UIControlState.normal)
        self.clipsToBounds      = true
        self.layer.masksToBounds = true
    }
    
    override var isHighlighted: Bool {
        didSet {
            if (isHighlighted) {
                self.backgroundColor = UIColor.red
                self.setTitleColor(udBlack, for: UIControlState.highlighted)
            }
            else {
                self.backgroundColor = UIColor.white
                self.setTitleColor(udYellow, for: UIControlState.highlighted)
            }
        }
    }
}
