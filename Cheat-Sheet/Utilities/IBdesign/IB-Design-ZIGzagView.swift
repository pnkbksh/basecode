//
//  IB-Design-ZIGzagView.swift
//  Cheat-Sheet
//
//  Created by Pooja Negi on 08/05/18.
//  Copyright © 2018 PNKBKSH. All rights reserved.
//

import UIKit

//MARK:- ZIG zag Design
@IBDesignable class zigZag: UIView {
    
    override func layoutSubviews() {
        super.layoutSubviews()
        zigZagUpdate()
    }
    @IBInspectable var zigzag: Bool = false {
        didSet {
            zigZagUpdate()
        }
    }
    
    @IBInspectable var zigzagWidth: CGFloat = 5.0 {
        didSet { zigZagUpdate() }
    }
    
    @IBInspectable var zigzagHeight: CGFloat = 3.0 {
        didSet { zigZagUpdate() }
    }
    
    
    func zigZagUpdate() {
        let width = self.frame.size.width
        let height = self.frame.size.height
        let zigZagWidth = zigzagWidth
        let zigZagHeight = zigzagHeight
        
        var yInitial = height-zigZagHeight
        let zigZagPath = UIBezierPath()
        zigZagPath.move(to: CGPoint(x:0, y:0))
        zigZagPath.addLine(to: CGPoint(x:0, y:yInitial))
        
        var slope = -1
        var x = CGFloat(0)
        var i = 0
        while x < width {
            x = zigZagWidth * CGFloat(i)
            let p = zigZagHeight * CGFloat(slope)
            let y = yInitial + p
            let point = CGPoint(x: x, y: y)
            zigZagPath.addLine(to: point)
            slope = slope*(-1)
            i += 1
        }
        
        zigZagPath.addLine(to: CGPoint(x:width,y: 0))
        
        // draw the line from top right to Bottom
        zigZagPath.addLine(to: CGPoint(x:width,y:height))
        
        yInitial = 0 + zigZagHeight
        x = CGFloat(width)
        i = 0
        while x > 0 {
            x = width - (zigZagWidth * CGFloat(i))
            let p = zigZagHeight * CGFloat(slope)
            let y = yInitial + p
            let point = CGPoint(x: x, y: y)
            zigZagPath.addLine(to: point)
            slope = slope*(-1)
            i += 1
        }
        
        // Now Close the path
        zigZagPath.close()
        let shapeLayer = CAShapeLayer()
        shapeLayer.path = zigZagPath.cgPath
        self.layer.mask = shapeLayer
    }
    
}
