//
//  ViewController-Sidemenu.swift
//  Cheat-Sheet
//
//  Created by Pooja Negi on 08/05/18.
//  Copyright © 2018 PNKBKSH. All rights reserved.
//

import Foundation
import UIKit


/*
 // MARK:- Side menu Details
 
 first check in app degte ->
     2 method. --->
 
             (1). make variable on top
 
             static var menu_VC :NavigationDrawerVC!  // sidemenu vc
             static var slideMenu_Bool:Bool = true
 
 
             (2). in did finishlaunc - assigne vc
             (NavigationDrawerVC vc name)
 
             DispatchQueue.main.async {
             AppDelegate.menu_VC = sideSB.instantiateViewController(withIdentifier: "NavigationDrawerVC") as! NavigationDrawerVC
             }
 

 
 2). NOW in Navigation or saperat class
 
             // changeViewController handle
             //enum Menu: Int {
             //    case Home
             //    case Track
             //    case Logout
             //}
 
             //protocol DrawerProtocol : class {
             //    func changeViewController(_ menu: Menu)
             //}
 
 
 
 
 3). in NavigationDrawerVC  class
 
         MARK:- Change View Controller on Click
         func changeViewController(_ menu: Menu) {
 
         switch menu {
         case .Home:
         closeSlideView()
         case .Track:
         funcjajdsalkjlk
         case .logout:
         logout func
         }
         }
 
             (a). add menu item variable :-
             tv inscance variable
             var menusTitle    = ["Home","abc","Logout"] // item
             var menuTVIcon    = ["k", "h", "l", "i","m","r"]


             (b) in table

             numberOfRowsInSection -> menu.count
             cellfor row --> self.menusTitle[indexPath.row]
             DiDselect ----->
             DispatchQueue.main.async(execute: {
             if let menu = Menu(rawValue: indexPath.row) {
             self.changeViewController(menu)
             tableView.deselectRow(at: indexPath, animated: true)
             }
             self.closeSlideView()
             })
 
 
 */

extension UIViewController{
    
    //set slide menu
    func setSlideView() {
        if AppDelegate.slideMenu_Bool == true{
            self.openSlideView()
        }
        else{
            self.closeSlideView()
        }
    }
    
    //Open Slide menu with animation
    func openSlideView(){
        DispatchQueue.main.async(execute: {
            UIView.animate(withDuration: 0.5) { ()-> Void in
                //                AppDelegate.menu_VC.view.frame = CGRect(x: 0, y:0, width: (UIScreen.main.bounds.width - 100) , height: UIScreen.main.bounds.height)
                AppDelegate.menu_VC.view.frame = CGRect(x: 0, y:0, width: (UIScreen.main.bounds.width) , height: UIScreen.main.bounds.height)
                
                self.addChildViewController(AppDelegate.menu_VC)
                self.view.addSubview(AppDelegate.menu_VC.view)
            }
            AppDelegate.slideMenu_Bool = false
        })
    }
    
    //close slide menu with animation
    func closeSlideView(){
        DispatchQueue.main.async(execute: {
            UIView.animate(withDuration: 0.5, animations: { ()-> Void in
                //                AppDelegate.menu_VC.view.frame = CGRect(x: -(UIScreen.main.bounds.width - 100), y: 0, width: (UIScreen.main.bounds.width - 100), height: UIScreen.main.bounds.height)
                
                AppDelegate.menu_VC.view.frame = CGRect(x: -(UIScreen.main.bounds.width), y: 0, width: (UIScreen.main.bounds.width), height: UIScreen.main.bounds.height)
                
            }) {(finished) in
                AppDelegate.menu_VC.view.removeFromSuperview()
            }
            AppDelegate.slideMenu_Bool = true
        })
    }
    
    //Set swipe gesture for left & right
    func swipeSlideMenu(){
        let swipeRight =  UISwipeGestureRecognizer(target: self, action: #selector(self.swipe_Method))
        swipeRight.direction =  UISwipeGestureRecognizerDirection.right
        
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(self.swipe_Method))
        swipeLeft.direction = UISwipeGestureRecognizerDirection.left
        
        self.view.addGestureRecognizer(swipeRight)
        self.view.addGestureRecognizer(swipeLeft)
    }
    
    //Swipe method
    @objc func swipe_Method(gesture: UISwipeGestureRecognizer){
        
        switch gesture.direction {
        case UISwipeGestureRecognizerDirection.right:
            print("Right")
            DispatchQueue.main.async(execute: { self.openSlideView()})
            
        case UISwipeGestureRecognizerDirection.left:
            print("Left")
            DispatchQueue.main.async(execute: { self.closeSlideView()})
        default:
            break
        }
    }
}
