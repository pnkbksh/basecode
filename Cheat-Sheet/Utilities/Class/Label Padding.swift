//
//  Label Padding.swift
//  Cheat-Sheet
//
//  Created by Pooja Negi on 08/05/18.
//  Copyright © 2018 PNKBKSH. All rights reserved.
//

import UIKit

//padding to label
class PadingToLabel:UILabel{
    
    let padding = UIEdgeInsets(top: 15, left: 15, bottom: 15, right: 15)
    
    override func drawText(in rect: CGRect) {
        super.drawText(in: UIEdgeInsetsInsetRect(rect, padding))
    }
    
    override func sizeThatFits(_ size: CGSize) -> CGSize {
        let superSizeThatFits = super.sizeThatFits(size)
        let width = superSizeThatFits.width + padding.left + padding.right
        let height = superSizeThatFits.height + padding.top + padding.bottom
        return CGSize(width: width, height: height)
    }
}

//Tost
class tostShow
{
    class private func showAlert(backgroundColor:UIColor, textColor:UIColor, message:String)
    {
        
        let appDelegate: AppDelegate = UIApplication.shared.delegate as! AppDelegate
        
        let label = PadingToLabel()
        label.frame =  CGRect(x: 0, y:  appDelegate.window!.frame.height-100, width: appDelegate.window!.frame.width-50, height:0)
        
        label.text = message
        label.backgroundColor =  backgroundColor
//        label.font = UIFont(name: Proxima_RegularFont, size: 13)
        label.textColor = textColor
        
        label.textAlignment = .center
        label.sizeToFit()
        label.layer.cornerRadius = 8
        label.clipsToBounds = true
        label.center = appDelegate.window!.center
        label.frame.origin.y = appDelegate.window!.frame.height-200
        label.alpha = 1
        
        appDelegate.window!.addSubview(label)
        UIView.animate(withDuration: 5.0, delay: 0.0, options:.curveEaseOut, animations: {
            label.alpha = 0.0
        }) { (isCompleted) in
            label.removeFromSuperview()
        }
    }
    
    
    class func showPositiveMessage(message:String)
    {
        showAlert(backgroundColor: UIColor.gray, textColor: UIColor.yellow, message: message)
    }
    
}
